import json
import os
import subprocess
import sys
import time
import traceback
from io import StringIO
from pathlib import Path
from urllib.parse import urlparse, urlunparse
import paramiko
import virtualenv
from njinn import NjinnAPI

result = None
njinn = None
IS_WINDOWS = sys.platform.startswith("win")


class GitEnabled:

    repository = {
        "url": None,
        "username": None,
        "password": None,
        "branch": None,
    }

    @staticmethod
    def sanitized_url(url):
        """
        Returns parts of the URL that most likely don't contain secrets
        """
        parsed_url = list(urlparse(url))
        """ 
        Indices used:
        https://docs.python.org/3/library/urllib.parse.html#urllib.parse.urlparse
        """
        netloc_parts = parsed_url[1].split("@")
        if len(netloc_parts) == 2:
            parsed_url[1] = f"******@{netloc_parts[1]}"
        elif len(netloc_parts) > 1:
            # be defensive, something unexpected happened
            parsed_url[1] = "******"
        # also erase parameters, queries or fragments
        parsed_url[3] = ""  # params
        parsed_url[4] = ""  # query
        parsed_url[5] = ""  # fragment
        return urlunparse(parsed_url)

    @staticmethod
    def sanitize_output(command_output, repo_url):
        """
        Replaces repo_url with a version that most likely does not contain secrets
        """
        if not repo_url:
            return command_output
        return command_output.replace(repo_url, GitEnabled.sanitized_url(repo_url))

    @staticmethod
    def print_sanitized_clone_output(kwargs):
        print(
            f'Cloning repository: {GitEnabled.sanitized_url(kwargs["url"])} from branch: {kwargs["branch"]}'
        )
        print(f'To path: {kwargs["to_path"]}')

    def get_kwargs(self):
        kwargs = {
            "url": self.repository.get("url"),
            "branch": "master",
            "to_path": "repository",
        }

        username = self.repository.get("username", "")
        password = self.repository.get("password", "")
        parsed_url = list(urlparse(kwargs["url"]))
        scheme = parsed_url[0]
        if (username or password) and scheme.startswith("http"):
            parsed_url[1] = f"{username}:{password}@{parsed_url[1]}"
            kwargs["url"] = urlunparse(parsed_url)

        if self.repository.get("branch"):
            kwargs["branch"] = self.repository.get("branch")
        return kwargs

    def clone_repository(self):
        if not self.repository or not self.repository.get("url"):
            return
        import git

        kwargs = self.get_kwargs()

        GitEnabled.print_sanitized_clone_output(kwargs)
        try:
            git.Repo.clone_from(**kwargs)
            os.chdir("repository")
        except Exception as e:
            print(GitEnabled.sanitize_output(str(e), kwargs["url"]))

    def clone_repository_cmd(self):
        """
        Returns a shell command to clone a repository, check exit code,
        and change directory.
        """

        if not self.repository or not self.repository.get("url"):
            return ""

        kwargs = self.get_kwargs()
        GitEnabled.print_sanitized_clone_output(kwargs)

        cmd = f'git clone {kwargs["url"]} {kwargs["to_path"]}'
        if kwargs["branch"]:
            cmd += f' --single-branch --branch {kwargs["branch"]}'

        cmd += "\nrc=$?; if [[ $rc != 0 ]]; then exit $rc; fi\n"
        cmd += f'cd {kwargs["to_path"]}\n'
        return cmd


class RunProcess(GitEnabled):
    """
    Could use this as a base class for specific process, eg java...
    which would search for an installation and adds meta data
    """

    cmd = ""

    def run(self):
        self.clone_repository()
        sys.stdout.flush()
        proc = subprocess.run(
            self.cmd,
            stderr=subprocess.STDOUT,
            shell=True,
            text=True,
            encoding="utf_8",
            errors="replace",
        )
        proc.check_returncode()


class RunScriptBase(GitEnabled):
    script = ""
    arguments = ""
    interpreter = "sh"

    def __init__(
        self,
        interpreter="sh",
        line_separator="\n",
        suffix=None,
        script_header=None,
        format="{interpreter} {arguments} {script}",
        script_footer=None,
    ):
        self.interpreter = interpreter
        self._line_separator = line_separator
        self._suffix = suffix
        self._script_header = script_header
        self._script_footer = script_footer
        self._format = format
        self._proc: subprocess.Popen = None

    def get_encoding(self):
        return "utf_8"

    def write_script(self):
        """
        Writes the script content to a file in the current directory
        """
        abspath = os.path.abspath(os.path.join("script" + (self._suffix or "")))

        # first normalize line separator, then change if required
        script = self.script.replace("\r", "")
        if self._line_separator != "\n":
            script = script.replace("\n", self._line_separator)

        if self._script_header:
            script = self._script_header + script

        if self._script_footer:
            script += self._script_footer

        with open(abspath, "w", encoding=self.get_encoding()) as fp:
            fp.write(script)

        return abspath

    def run(self):
        self.clone_repository()
        arguments = self.arguments or ""
        script = self.write_script()
        cmd = self._format.format(
            interpreter=self.interpreter, arguments=arguments, script=script
        )
        sys.stdout.flush()
        self._proc = subprocess.Popen(
            cmd,
            stderr=subprocess.STDOUT,
            shell=True,
            text=True,
            encoding="utf_8",
            errors="replace",
        )
        self._proc.wait()
        self.check_returncode()

    def check_returncode(self):
        """Raise CalledProcessError if the exit code is non-zero."""
        if self._proc and self._proc.returncode:
            raise subprocess.CalledProcessError(
                self._proc.returncode,
                self._proc.args,
                self._proc.stdout,
                self._proc.stderr,
            )

    def on_kill(self):
        if self._proc:
            if not IS_WINDOWS:
                pg = os.getpgid(self._proc.pid)
                print(
                    f"Killing process group {pg} of action subproces {self._proc.pid}."
                )
                os.killpg(pg, 9)
            else:
                print("Killing subproces", self._proc.pid)
                self._proc.kill()


class RunScript(RunScriptBase):
    """
    Writes a script and executes it with a given script interpreter
    """

    script = ""
    arguments = ""
    interpreter = "sh"


class RunWindowsBatchScript(RunScriptBase):
    """
    Writes a Windows batch script and executes it
    """

    script = ""
    arguments = ""

    def __init__(self):
        super().__init__(
            interpreter="cmd.exe",
            suffix=".bat",
            line_separator="\r\n",
            script_header="@echo off\r\n",
            # windows needs quotes for path with spaces
            format='{interpreter} {arguments} /c "{script}"',
        )

    def get_encoding(self):
        if self._njinn and self._njinn.config:
            return getattr(self._njinn.config, "codepage", "utf_8")
        return "utf_8"


class RunPowershellScript(RunScriptBase):
    """
    Writes a Powershell batch script and executes it
    """

    script = ""
    arguments = ""

    def __init__(self):
        super().__init__(
            interpreter="powershell.exe",
            suffix=".ps1",
            line_separator="\r\n",
            script_header="""$result = @{}"""
            + "\r\n"
            + """$working_dir = $PWD.Path"""
            + "\r\n",
            script_footer="\r\n"
            # ConvertTo-Json produces empty string instead of empty JSON object in case of $null
            + "if ($result -eq $null) { $result = @{} }" + "\r\n"
            # PowerShell pipe converts single list items into objects, this is why conversion happens as assignment not as pipe
            + """$result_json = ConvertTo-Json $result"""
            + "\r\n"
            + """$result_json | Out-File -FilePath $working_dir\\njinn_result.json -Encoding UTF8 """,
            # windows needs quotes for path names with spaces
            format='{interpreter} {arguments} -NonInteractive -ExecutionPolicy Bypass -File "{script}"',
        )

    def get_encoding(self):
        if self._njinn and self._njinn.config:
            return getattr(self._njinn.config, "codepage", "utf_8")
        return "utf_8"

    def run(self):
        super().run()
        result = None

        try:
            with open("njinn_result.json", encoding="utf-8-sig") as result_file:
                result = json.load(result_file)
        except:
            pass

        return result


class RunSecureScript(GitEnabled):
    """
    Creates connection to SSH server with provided credentials and executes input script
    """

    script = ""
    interpreter = "sh"
    sshconnection = {
        "host": None,
        "user": None,
        "password": None,
        "key": None,
        "key_passphrase": None,
        "encoding": None,
    }

    @property
    def encoding(self):
        return self.sshconnection.get("encoding", "utf-8")

    def run(self):
        if (
            not self.sshconnection
            and not self.sshconnection.get("host")
            and not self.sshconnection.get("name")
        ):
            return

        ssh_client = self._connect()
        clone_cmd = self.clone_repository_cmd()

        print(f"Using encoding: {self.encoding}")

        try:
            chan = ssh_client.get_transport().open_session()
            chan.set_combine_stderr(True)
            run_cmd = clone_cmd
            run_cmd += self.script.replace("\r", "")
            encoded = run_cmd.encode(self.encoding)
            chan.exec_command(encoded)
            stdout = self._read_paramimko_stream(chan.recv)
            exit_code = chan.recv_exit_status()
            try:
                print(stdout)
            except UnicodeEncodeError:
                print(
                    f"Cannot print the output by configured encoding '{self.encoding}'. Fallback to terminal's encoding '{sys.stdout.encoding}'"
                )
                print(
                    stdout.encode(sys.stdout.encoding, "replace").decode(
                        sys.stdout.encoding
                    )
                )
            print(f"Exit code {exit_code}")
            if exit_code != 0:
                raise Exception(f"Nonzero exit code: {exit_code}")
        finally:
            self._clean_up(ssh_client, clone_cmd)

    def _read_paramimko_stream(self, recv_func):
        result = b""
        buf = recv_func(1024)

        while buf != b"":
            result += buf
            buf = recv_func(1024)

        return result.decode(self.encoding, "replace")

    def _clean_up(self, ssh_client, clone_cmd):
        def read_std(x):
            return x.read().decode(self.encoding, "replace")

        repo_dir = self.get_kwargs()["to_path"]
        _, stdout, _ = ssh_client.exec_command(f"find . -type d -name {repo_dir}")
        repo_found = read_std(stdout)
        if repo_found:
            _, _, stderr = ssh_client.exec_command(f"rm -r {repo_dir}")
            stderr = read_std(stderr)
            if stderr:
                print(f"Error during cleaning up: {stderr}")

        ssh_client.close()
        print("\nConnection closed")

    def _connect(self):
        """
        Returns SSH client instance 
        """

        host = self.sshconnection["host"]
        port = 22  # default ssh port
        if ":" in self.sshconnection["host"]:
            host, port = self.sshconnection["host"].split(":")
        print(f"Creating SSH connection to {host}:{port}")

        password = self.sshconnection.get("password")
        pass_phrase = self.sshconnection.get("key_passphrase")
        private_key = self.sshconnection.get("key")
        pkey = None
        if private_key:
            pkey = paramiko.RSAKey.from_private_key(
                StringIO(private_key), password=pass_phrase
            )

        ssh_client = paramiko.SSHClient()
        ssh_client.load_system_host_keys()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            ssh_client.connect(
                host,
                port=port,
                username=self.sshconnection["user"],
                password=password,
                pkey=pkey,
                passphrase=pass_phrase,
                timeout=5,
            )
        except Exception as e:
            print("Error during connection", e)
            raise e

        return ssh_client


class RunPythonScript(GitEnabled):
    """
    Runs a python script and prepares a virtual environment if required
    to install pip requirements
    """

    script = ""
    script_path = None
    requirements = ""
    requirements_path = None

    def write_script(self):
        """
        Writes the script content to a temporary file
        """

        self.script_path = os.path.abspath("script.py")

        with open(self.script_path, "w", encoding="utf_8") as fp:
            fp.write(self.script)
        return self.script_path

    def write_requirements(self):
        """
        Writes the requirements.txt file if content is given
        """

        if not self.requirements:
            return None

        self.requirements_path = os.path.abspath("njinn_requirements.txt")

        with open(self.requirements_path, "w") as fp:
            fp.write(self.requirements)
        return self.requirements_path

    def setup_venv(self):

        if not self.requirements_path:
            return

        venv_path = os.path.normpath(os.path.abspath("./venv"))
        virtualenv.create_environment(venv_path, clear=True)

        # https://virtualenv.pypa.io/en/latest/userguide/#windows-notes
        binary_dir = "Scripts" if sys.platform.startswith("win") else "bin"
        activate_this = (Path(venv_path) / binary_dir / "activate_this.py").resolve()
        pip_executable = "pip.exe" if sys.platform.startswith("win") else "pip"
        cmd = [
            str((Path(venv_path) / binary_dir / pip_executable).resolve()),
            "--disable-pip-version-check",
            "install",
            "-r",
            self.requirements_path,
        ]
        exec(open(activate_this).read(), {"__file__": activate_this})
        pipresult = subprocess.run(
            cmd,
            universal_newlines=True,
            stderr=subprocess.STDOUT,
            text=True,
            encoding="utf_8",
            errors="replace",
        )
        pipresult.check_returncode()

    def get_encoding(self):
        if self._njinn and self._njinn.config:
            return getattr(self._njinn.config, "codepage", "utf_8")
        return "utf_8"

    def run(self):
        self.clone_repository()
        global result
        result = {}
        global njinn
        njinn = self._njinn

        self.write_script()

        if self.requirements:
            self.write_requirements()
            self.setup_venv()

        print("")
        sys.path.insert(0, os.getcwd())

        script = (Path("script.py")).resolve()
        sys.stdout.flush()
        try:
            if (
                not "pytest" in sys.modules
            ):  # workaround for https://github.com/pytest-dev/pytest/issues/4843
                sys.stdout.reconfigure(encoding=self.get_encoding(), errors="replace")
                sys.stderr.reconfigure(encoding=self.get_encoding(), errors="replace")
            exec(open(script, encoding="utf-8").read(), globals())
        except Exception as e:
            """
            Our goal is to provide an Exception with a meaningful message.
            Since we're running Python code, the interesting part is the
            traceback of the user's code.

            We don't want to bother the user with the parts specific to the 
            Njinn Python action, so we skip stack frame until the last 
            occurence of this file.
            """
            tb = traceback.format_exc()
            lines = tb.split("\n")
            if len(tb) > 1:
                max_index = -1
                for idx, line in enumerate(lines):
                    if 'shell.py", line' in line:
                        max_index = idx
                    if (
                        "During handling of the above exception, another exception occurred:"
                        in line
                    ):
                        # Don't cut off previous exceptions.
                        break
                # Exception now has the relevant parts of the traceback as its message.
                raise Exception(
                    "\n".join([lines[0]] + lines[max_index + 2 :])
                ) from None
            else:
                raise e

        if isinstance(result, dict):
            return result
        else:
            return None


class Sleep:
    """
    This action sleeps for given amount of seconds.
    """

    seconds = 1

    def run(self):
        try:
            self.seconds = int(self.seconds)
            self.seconds = 0 if self.seconds < 0 else self.seconds
        except ValueError:
            self.seconds = 0

        print(f"Sleeping for {self.seconds} seconds")
        time.sleep(self.seconds)

        return self.seconds
