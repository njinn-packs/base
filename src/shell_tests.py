from contextlib import redirect_stdout
from io import StringIO
from os import chdir
from tempfile import TemporaryDirectory
from unittest.mock import patch

# git package is not directly used in tests but it's essential for running them
import git

from shell import GitEnabled, RunPythonScript

REPO_URL = "https://secret:secret@github.com/octocat/Spoon-Knife.git"
COMMAND_OUTPUT = """
Cmd('git') failed due to: exit code(128)
cmdline: git clone --branch=master -v https://secret:secret@github.com/octocat/Spoon-Knife.git repository
stderr: 'Cloning into 'repository'...
fatal: something bad happened
'
"""


def inside_temp_dir(func):
    with TemporaryDirectory() as td:
        chdir(td)
        func()


def test_sanitize_url():
    ge = GitEnabled()
    assert (
        ge.sanitized_url(
            "https://user:secret@github.com/path#token?hidden=confidential"
        )
        == "https://******@github.com/path"
    )
    assert (
        ge.sanitized_url("git://user:secret@github.com/path#token?hidden=confidential")
        == "git://******@github.com/path"
    )
    no_secrets = "https://github.com/path"
    assert ge.sanitized_url(no_secrets) == no_secrets


def test_sanitize_output():
    assert not "secret" in GitEnabled.sanitize_output(COMMAND_OUTPUT, REPO_URL)


@inside_temp_dir
def test_python():
    s = StringIO()
    with redirect_stdout(s):
        py = RunPythonScript()
        py.script = 'print("hello world from python")\n'
        py._njinn = None
        py.run()
    assert "\nhello world from python\n" == s.getvalue()


def test_git_no_clone_required():
    ge = GitEnabled()
    with patch("git.Repo.clone_from") as cf:
        ge.clone_repository()
        cf.assert_not_called()


def clone(side_effect):
    ge = GitEnabled()
    ge.repository["url"] = REPO_URL
    ge.repository["branch"] = "master"
    s = StringIO()
    with redirect_stdout(s):
        with patch("git.Repo.clone_from", side_effect=side_effect) as clone:
            with patch("os.chdir") as cd:
                ge.clone_repository()
                # clone called with correct params, then cd-ing into directory
                clone.assert_called_once_with(
                    url=REPO_URL, branch="master", to_path="repository"
                )
                if not side_effect:
                    cd.assert_called_once_with("repository")
    assert (
        "Cloning repository: https://******@github.com/octocat/Spoon-Knife.git from branch: master"
        in s.getvalue()
    )
    assert "secret" not in s.getvalue()
    return s.getvalue()


def test_git_local_success():
    clone(None)


def test_git_local_failure():
    output = clone(Exception(COMMAND_OUTPUT))
    assert "git clone --branch=master" in output


def test_git_remote():
    ge = GitEnabled()
    ge.repository["url"] = REPO_URL
    ge.repository["branch"] = "master"
    s = StringIO()
    with redirect_stdout(s):
        cmd = ge.clone_repository_cmd()
    assert (
        "Cloning repository: https://******@github.com/octocat/Spoon-Knife.git from branch: master"
        in s.getvalue()
    )
    assert cmd.startswith(
        "git clone https://secret:secret@github.com/octocat/Spoon-Knife.git repository --single-branch --branch master"
    )
