from ftplib import FTP, FTP_TLS, error_perm
from io import StringIO
import logging
import os
import posixpath
from pathlib import Path, PurePosixPath
import stat
import time
import fnmatch
import paramiko
import sys


class FTPBase:
    conn = None
    transport = None
    protocol = "ftp"

    def __init__(self):
        logging.basicConfig()
        self.logger = logging.getLogger(__name__)
        log_level = os.getenv("NJINN_LOG_LEVEL", "INFO").upper()
        if not log_level:
            log_level = "INFO"
        self.logger.setLevel(log_level)

    def _descend(self, path, create_path=False):
        self.logger.debug(f"Changing into {path}")
        dirs = path.split("/")
        for directory in dirs:
            if self.protocol == "sftp":
                try:
                    self.conn.chdir(directory)
                except IOError as e:
                    if create_path:
                        print(f"Folder '{directory}' didn't exist, creating folder.")
                        self.conn.mkdir(directory)
                        self.conn.chdir(directory)
                    else:
                        print(f"Folder '{directory}' doesn't exist.")
                        raise e
            else:
                try:
                    self.conn.cwd(directory)
                except error_perm as e:
                    if e.args[0][:3] == "550":
                        if create_path:
                            print(
                                f"Folder '{directory}' didn't exist, creating folder."
                            )
                            self.conn.mkd(directory)
                            self.conn.cwd(directory)
                        else:
                            print(f"Folder '{directory}' doesn't exist.")
                            raise e
                    else:
                        raise e

        if self.protocol == "sftp":
            return self.conn.getcwd()
        else:
            return self.conn.pwd()

    def _chdir(self, path):
        if self.protocol == "sftp":
            self.conn.chdir(path)
        else:
            self.conn.cwd(path)

    def _mkdir(self, name):
        if self.protocol == "sftp":
            self.conn.mkdir(name)
        else:
            self.conn.mkd(name)

    def _pwd(self):
        if self.protocol == "sftp":
            return self.conn.normalize(".")
        else:
            return self.conn.pwd()

    def connect(self, host="", user="", password="", protocol="", port=0, active=False, private_key=None, pass_phrase=None):
        if protocol:
            self.protocol = protocol

        print(f"Connecting to {self.protocol}:{host} as {user}")
        if self.protocol == "ftp":
            if not port:
                port = 21
            self.conn = FTP()
            self.conn.connect(host, port)
            self.conn.login(user, password)
        elif self.protocol == "ftps":
            if not port:
                port = 21
            self.conn = FTP_TLS()
            self.conn.connect(host, port)
            self.conn.login(user, password)
            self.conn.prot_p()
        elif self.protocol == "sftp":
            if not port:
                port = 22
            pkey = None
            if private_key:
                pkey = paramiko.RSAKey.from_private_key(
                    StringIO(private_key), password=pass_phrase
                )

            self.transport = paramiko.Transport((host, port))

            try:
                self.transport.connect(username=user, password=password, pkey=pkey)
                self.conn = paramiko.SFTPClient.from_transport(self.transport)
            except Exception as e:
                print("Error during connection", e)
                raise e
        else:
            raise Exception(f"Unsupported protocol {self.protocol}")

        if active and (self.protocol != "sftp"):
            print("Switching to active mode")
            self.conn.set_pasv(False)

    def disconnect(self):
        if self.protocol == "sftp":
            if self.conn:
                self.conn.close()
            if self.transport:
                self.transport.close()
        else:
            try:
                self.conn.quit()
            except Exception as e:
                print(f"There was an error while sending the quit message. {e}")
        print("Connection closed.")

    def delete_file(self, file_name=""):
        # assumes already changed into remote folder location of file to delete
        print(f"Deleting: {file_name}")
        if self.protocol == "sftp":
            self.conn.remove(file_name)
        else:
            path, name = posixpath.split(file_name)
            current_path = self.conn.pwd()
            self.conn.cwd(path)
            self.conn.delete(name)
            self.conn.cwd(current_path)

    def remove_dir(self, path=""):
        print(f"Removing directory '{path}'")
        base_path, dir = posixpath.split(path)
        if self.protocol == "sftp":
            current_path = self.conn.normalize(".")
            self.conn.chdir(base_path)
            try:
                self.conn.rmdir(dir)
            except Exception as e:
                print(f"Error during removal of directory '{path}', moving on.\n {e}")
            self.conn.chdir(current_path)
        else:
            current_path = self.conn.pwd()
            self.conn.cwd(base_path)
            try:
                self.conn.rmd(dir)
            except Exception as e:
                print(f"Error during removal of directory '{path}', moving on.\n {e}")
            self.conn.cwd(current_path)

    def makedirs(self, path):
        # like os.makedirs and change into path
        ppath = PurePosixPath(path)

        for part in ppath.parts:
            try:
                self._chdir(part)
            except:
                self._mkdir(part)
                self._chdir(part)

    def upload_file(self, target_path="", source_file="", base_path=""):
        # upload source_file and create dir structure relative to base_path in target_path if required
        current_path = self._pwd()

        self.logger.debug(f"Uploading {source_file} in {base_path} to {target_path}")
        abs_source_path = os.path.join(Path(base_path), Path(source_file))
        rel_source_path, name = os.path.split(source_file)
        self._chdir(target_path)
        self.makedirs(Path(rel_source_path).as_posix())

        with open(abs_source_path, "rb") as file:
            print(f"Uploading '{abs_source_path}' ...", end="")
            if self.protocol == "sftp":
                self.conn.putfo(file, name)
            else:
                self.conn.storbinary("STOR " + name, file)
            print(" done.")

        self._chdir(current_path)

    def download_file(self, target_path="", source_file=""):
        path, file_name = posixpath.split(source_file)
        full_path = os.path.join(Path(target_path), Path(path))
        os.makedirs(full_path, exist_ok=True)
        self.logger.debug(os.path.join(full_path, file_name))
        with open(os.path.join(full_path, file_name), "wb") as file:
            print(f"Downloading '{source_file}' ...", end="")
            if self.protocol == "sftp":
                self.conn.getfo(source_file, file)
            else:
                self.conn.retrbinary("RETR " + source_file, file.write)
            print(" done.")

    def list_dir(self):
        result = dict()
        if self.protocol == "sftp":
            entries = self.conn.listdir_attr()
            for entry in entries:
                # emulate FTP mlsd result for SFTP
                type = ""
                if stat.S_ISDIR(entry.st_mode):
                    type = "dir"
                else:
                    type = "file"

                result[entry.filename] = {
                    "type": type,
                    "size": entry.st_size,
                    "modify": entry.st_mtime,
                }

            return result
        else:
            # TODO fallback solution in case only nlst is supported
            entries = self.conn.mlsd()
            for entry, meta in entries:
                if entry == "." or entry == "..":
                    continue
                result[entry] = meta
            return result

    def walk(self, item, recursive=False):
        # walk directory tree without impact on current directory
        current_path = self._pwd()
        self.logger.debug(f"walking {item} in {current_path}")
        result = self._walk(current_path, item, recursive)
        self._chdir(current_path)
        return result

    def _walk(self, abs_base_path, rel_path, recursive=False):
        # walk directory tree for src_path
        result = dict()
        self.logger.debug(f"stepping into {abs_base_path}/{rel_path}")
        self._chdir(f"{abs_base_path}/{rel_path}")
        entries = self.list_dir()
        # check if path exists
        for entry in entries:
            path = f"{rel_path}/{entry}"
            self.logger.debug(f"found item {path}")

            # depth first to allow for directory removal for delete after transfer option
            if entries[entry]["type"] == "dir" and not recursive:
                continue
            if entries[entry]["type"] == "dir" and recursive:
                self.logger.debug(f"{entry} is a directory")
                # add items of subdirectories
                result = {**result, **self._walk(abs_base_path, path, recursive)}
                # add directory itself
                result[path] = entries[entry]
            # add file or directory itself
            result[path] = entries[entry]

        return result


class FTPFileUpload(FTPBase):
    source_path = ""
    target = {}
    target_path = ""
    recursive = False

    def _matches(self, pattern, entries, base_path):
        files = {}

        for entry in entries:
            name = entry.name
            self.logger.debug(f"Matching: {name}")
            if fnmatch.fnmatch(name, pattern):
                self.logger.debug(f"{name} is a match, adding item")
                files[Path(entry.path).relative_to(base_path).as_posix()] = entry

                if os.path.isdir(entry) and self.recursive:
                    self.logger.debug(
                        f"and {name} is a directory, walking subdir with recursive set to {self.recursive}"
                    )
                    files = {
                        **files,
                        **self._walk_local(base_path, entry.path, self.recursive),
                    }

        return files

    def _walk_local(self, base_path, path, recursive=False):
        items = dict()
        self.logger.debug(f"Walking {path}")
        dir_items = os.scandir(path)
        for dir_item in dir_items:
            self.logger.debug(f"found item {dir_item.path}")
            if os.path.isdir(dir_item) and not recursive:
                continue
            if os.path.isdir(dir_item) and recursive:
                self.logger.debug(f"Step into {dir_item.path}")
                items = {
                    **items,
                    **self._walk_local(base_path, dir_item.path, recursive),
                }
                items[Path(dir_item.path).relative_to(base_path).as_posix()] = dir_item
            items[Path(dir_item.path).relative_to(base_path).as_posix()] = dir_item
        return items

    def _upload(self, base_path, pattern):
        if not os.path.exists(base_path):
            raise Exception(f"Source '{base_path}' can not be found, aborting.")

        print(f"Looking for files matching '{pattern}' in '{base_path}' to upload")
        entries = os.scandir(base_path)
        items = self._matches(pattern, entries, base_path)
        print(f"Found {len(items)} item(s) to upload")

        for item in list(items.keys()):
            self.logger.debug(f"Looking at {item}")
            if os.path.isfile(items[item]):
                self.logger.debug(f"Uploading '{item}'")
                self.upload_file(self.target_path, item, base_path)
            if os.path.isdir(items[item]):
                # do not process directories if recursive is disabled
                if not self.recursive:
                    print(f"Recursive not enabled, omitting directory '{item}'")
                    del items[item]
                else:
                    # in case its an empty dir and therefore wasn't created by upload_file
                    path = Path(posixpath.join(self.target_path, item)).as_posix()
                    # TODO check for existence and only create if nonexistent (doesnt fail for existing, but would be result in better output)
                    print(f"Creating directory {path}")
                    self.makedirs(path)

        print("Finished uploading. \n")

        return {"items": list(items.keys())}

    def run(self):
        if not self.source_path:
            raise Exception("No source path provided!")

        if not self.target.get("host"):
            raise Exception("No target host provided!")

        if self.recursive:
            print("Recursively upload folder tree")

        self.target["protocol"] = self.target.get("protocol", "ftp")

        try:
            self.connect(
                self.target["host"],
                user=self.target["user"],
                password=self.target.get("password"),
                protocol=self.target["protocol"],
                port=self.target.get("port", 0),
                active=self.target.get("active", False),
                private_key=self.target.get("key"),
                pass_phrase=self.target.get("key_passphrase"),
            )

            if not self.source_path or self.source_path == "/":
                self.source_path += "*"
            elif len(self.source_path) > 1 and self.source_path[-1] == "/":
                self.source_path = self.source_path[:-1]

            src_path, src_filename = os.path.split(self.source_path)
            self.logger.debug(f"Source path: {src_path} and pattern {src_filename}")

            print(f"Changing into target path '{self.target_path}'")
            self._descend(
                self.target_path, not self.target.get("fail_path_unavailable", False)
            )
            result = self._upload(src_path, src_filename)

            return result

        finally:
            self.disconnect()


class FTPFileDownload(FTPBase):
    source = {}
    source_path = ""
    target_path = ""
    file_stable = False
    file_stable_interval = 60
    delete_transferred = False
    recursive = False

    def _matches(self, pattern, entries):
        files = {}

        for entry in entries:
            name = entry
            meta = entries[entry]
            self.logger.debug(f"Matching: {name}")
            if fnmatch.fnmatch(name, pattern):
                self.logger.debug(f"{name} is a match, adding item")
                files[name] = meta

                if meta["type"] == "dir" and self.recursive:
                    self.logger.debug(
                        f"and {name} is a directory, walking subdir with recursive set to {self.recursive}"
                    )
                    files = {**files, **self.walk(name, self.recursive)}

        return files

    def _download(
        self,
        pattern,
        file_stable=False,
        file_stable_interval=60,
        delete_transferred=False,
    ):
        if not os.path.exists(self.target_path):
            raise Exception(f"Target '{self.target_path}' can not be found, aborting.")

        print(f"Looking for files matching '{pattern}' to download")

        entries = self.list_dir()
        files = self._matches(pattern, entries)
        print(f"Found {len(files)} item(s) to download")

        if file_stable:
            print(
                f"Waiting for {file_stable_interval} seconds to verify files are stable on source"
            )
            time.sleep(file_stable_interval)

            print("Verifying files to download")
            entries2 = self.list_dir()
            files2 = self._matches(pattern, entries2)

            for file, meta in files.items():
                try:
                    self.logger.debug(f"Item Metadata: {meta}")
                    meta2 = files2.pop(file)
                    if meta["type"] != "dir" and meta2["size"] != meta["size"]:
                        raise Exception(
                            f"{file} filesize changed from {meta['size']} to {meta2['size']}, aborting!"
                        )
                    if meta2["modify"] != meta["modify"]:
                        raise Exception(
                            f"{file} modification date changed. Before {meta['modify']}, now {meta2['modify']}, aborting!"
                        )
                except KeyError:
                    raise Exception(f"{file} can't be found anymore, aborting!")
            if files2:
                raise Exception(f"Found new items {list(files2)}, aborting!")

            print("Files stable, starting download")

        for file in list(files.keys()):
            if files[file]["type"] == "file":
                self.download_file(self.target_path, file)
            if files[file]["type"] == "dir":
                # do not process directories if recursive is disabled
                if not self.recursive:
                    print(f"Recursive not enabled, omitting directory '{file}'")
                    del files[file]
                else:
                    # in case its an empty dir and therefore wasn't created by download_file
                    path = Path(os.path.join(self.target_path, file))
                    if not path.exists():
                        print(f"Creating directory {file}")
                        os.makedirs(path, exist_ok=True)
                    else:
                        print(f"Directory {file} already exists")
        print("Finished downloading. \n")

        if delete_transferred:
            print("Removing transferred files from source including empty directories.")
            for file in files:
                # skip item if its a directory selection without wildcards
                if file == pattern:
                    continue
                if files[file]["type"] == "file":
                    self.delete_file(file)
                    continue
                if files[file]["type"] == "dir":
                    self.remove_dir(file)

        return {"items": list(files)}

    def run(self):
        if not self.target_path:
            raise Exception("No target path provided!")

        if not self.source.get("host"):
            raise Exception("No source host provided!")

        self.source["protocol"] = self.source.get("protocol", "ftp")

        if self.recursive:
            print("Recursively download folder tree")

        if not self.source_path or self.source_path == "/":
            self.source_path += "*"
        elif len(self.source_path) > 1 and self.source_path[-1] == "/":
            self.source_path = self.source_path[:-1]
        self.logger.debug(f"Source path: {self.source_path}")

        try:
            self.connect(
                self.source["host"],
                user=self.source["user"],
                password=self.source.get("password"),
                protocol=self.source["protocol"],
                port=self.source.get("port", 0),
                active=self.source.get("active", False),
                private_key=self.source.get("key"),
                pass_phrase=self.source.get("key_passphrase"),
            )

            src_path, src_filename = posixpath.split(self.source_path)

            print(f"Changing into source path '{src_path}'")
            self._descend(src_path)

            result = self._download(
                src_filename,
                self.file_stable,
                self.file_stable_interval,
                self.delete_transferred,
            )

            return result

        finally:
            self.disconnect()

