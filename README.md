# Base Action Pack

Njinn commonly used Actions

- Run Process
- Run Script

# After dependency changes

To avoid compatibility issues caused by updated downstream dependencies, we always freeze our dependencies before delivering the pack. We can then test those very dependencies on Windows and Linux. In the past, we've had lots of problems with new version of packages that suddenly required build tools on Windows.

## Howto

- `pipenv lock -r > requirements.txt` to freeze only the non-dev dependencies.
- remove the line `-i https://pypi.python.org/simple` from `requirements.txt`. This is enforced by the gitlab pipeline.
- remove the `bcrypt` dependency from `requirements.txt` - it's an optional dependency of cryptography, which we don't need but that causes trouble on Windows. This is enforced by the gitlab pipeline.
- Check `pipenv graph` to sanity check the contents of `requirements.txt`.
- Commit the updated `requirements.txt`.
- Ensure dependency installation on Linux and Windows.
